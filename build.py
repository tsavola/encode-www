#!/usr/bin/env python

from sys import argv
from os import listdir, makedirs
from os.path import join, exists, isdir, dirname, basename
from glob import glob
from xml.dom.minidom import parse

class Page:
	position = None

	def __init__(self, directory, filename):
		path = join(directory, filename)

		self.document = parse(path)

		position = self.document.documentElement.getAttribute('position')
		if position:
			self.position = int(position)

		self.basename, typename = filename.rsplit('.', 1)

	def get_title(self):
		return self.document.documentElement.getAttribute('title')

	def get_stylesheet(self):
		return self.document.documentElement.getAttribute('stylesheet')

	def get_content_nodes(self):
		return self.document.documentElement.childNodes

	def __cmp__(self, other):
		a = self.position, self.basename
		b = other.position, other.basename

		if a < b:
			return -1
		elif a > b:
			return 1
		else:
			return 0

	def __str__(self):
		if self.position is None:
			return 'hidden:%s' % self.basename
		else:
			return '%s:%s' % (self.position, self.basename)

class Template:
	def __init__(self, path):
		self.document = parse(path)
		basename, self.typename = path.rsplit('.', 1)

	def apply(self, page, navigation_pages):
		result = Result(self.document, self.build_filename(page.basename))

		title = page.get_title()
		content = page.get_content_nodes()
		navigation = self._build_navigation_nodes(result, navigation_pages, page)

		node = self._build_stylesheet_node(result, 'common.css')
		stylesheets = [node]

		filename = page.get_stylesheet()
		if filename:
			node = self._build_stylesheet_node(result, filename)
			stylesheets.append(node)

		result.insert_text('title-placeholder', title)
		result.insert_nodes('content-placeholder', content)
		result.insert_nodes('stylesheet-placeholder', stylesheets)
		result.insert_nodes('navigation-placeholder', navigation)

		return result

	def _build_stylesheet_node(self, result, filename):
		url = result.build_link(filename)

		link = result.document.createElement('link')
		link.setAttribute('rel', 'stylesheet')
		link.setAttribute('type', 'text/css')
		link.setAttribute('href', url)
		return link

	def _build_navigation_nodes(self, result, pages, active_page):
		nodes = []

		for page in pages:
			item = result.document.createElement('li')
			if page == active_page:
				item.setAttribute('class', 'active')
			nodes.append(item)

			url = result.build_link(self.build_filename(page.basename))

			link = result.document.createElement('a')
			link.setAttribute('href', url)
			item.appendChild(link)

			title = result.document.createTextNode(page.get_title())
			link.appendChild(title)

		return nodes

	def build_filename(self, basename):
		return '%s.%s' % (basename, self.typename)

class Result:
	def __init__(self, original, filename):
		self.document = original.cloneNode(deep=True)
		self.filename = filename

		for img in self.document.getElementsByTagName('img'):
			src = img.getAttribute('src')
			if src.find('/') < 0:
				src = self.build_link(src)
				img.setAttribute('src', src)

	def build_link(self, filename):
		directory = '../' * self.filename.count('/')
		return join(directory, filename)

	def insert_nodes(self, position, nodes):
		for placeholder in self.document.getElementsByTagName(position):
			parent = placeholder.parentNode

			for node in nodes:
				copy = node.cloneNode(deep=True)
				parent.insertBefore(copy, placeholder)

			parent.removeChild(placeholder)

	def insert_text(self, position, text):
		node = self.document.createTextNode(text)
		self.insert_nodes(position, (node,))

def scan_tree(directory, suffix, matches=[]):
	for name in listdir(directory):
		path = join(directory, name)
		if isdir(path):
			scan_tree(path, suffix, matches)
		elif name.endswith(suffix):
			matches.append(path)

	return matches

if __name__ == '__main__':
	sourcedir = argv[1]
	targetdir = argv[2]
	templatefile = argv[3]

	paths = scan_tree(sourcedir, '.xml')
	filenames = [path.split('/', 1)[1] for path in paths]

	all_pages = [Page(sourcedir, filename) for filename in filenames]
	all_pages.sort()

	navigation_pages = [page for page in all_pages if page.position is not None]

	template = Template(templatefile)

	for page in all_pages:
		print "Building", page

		result = template.apply(page, navigation_pages)

		path = join(targetdir, result.filename)
		directory = dirname(path)

		if not exists(directory):
			makedirs(directory)

		file = open(path, 'w')
		result.document.writexml(file, encoding='UTF-8')
		file.close()
