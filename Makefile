API_VERSION	= 0.5.3
API_MODULE	= encode
API_REPO	= ../encode

WWW_HOST	= shell.sourceforge.net
WWW_ROOT	= /home/groups/e/en/encode

default: htdocs
all: sources api htdocs

sources:
	-rm -rf sources
	git clone --local --shared $(API_REPO) sources
	cd sources && git reset --hard $(API_VERSION)
	sed -i "s/^PROJECT_NUMBER[[:space:]]*=.*/PROJECT_NUMBER=$(API_VERSION)/" sources/Doxyfile

api:
	cd sources && doxygen
	mkdir -p htdocs
	rm -rf htdocs/api
	mv sources/doc/html htdocs/api

htdocs:
	./build.py content htdocs content/template.xhtml

clean:
	-rm htdocs/*.xhtml
	-rm -r htdocs/api
	-rm -rf sources

publish:
	ssh $(WWW_HOST) "cd $(WWW_ROOT) && rm -rf htdocs.new && mkdir htdocs.new"
	tar cfj - -C htdocs . | ssh $(WWW_HOST) "tar xfj - -C $(WWW_ROOT)/htdocs.new/"
	ssh $(WWW_HOST) "cd $(WWW_ROOT) && rm -rf htdocs.old && mv htdocs htdocs.old && mv htdocs.new htdocs"

.PHONY: default all sources api htdocs clean publish
